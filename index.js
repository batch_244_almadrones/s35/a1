const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;
mongoose.connect(
  "mongodb+srv://admin:admin@zuitt-course-booking.rtqczgx.mongodb.net/b244-to-do?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => {
  console.log("We're connected to the database");
});

const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: "pending",
  },
});

const Task = mongoose.model("Task", taskSchema);

const userSchema = new mongoose.Schema({
  userName: String,
  password: String,
});

const User = mongoose.model("User", userSchema);

app.use(express.json());
//read data coming from the forms in front-end
app.use(express.urlencoded({ extended: true }));

//Business Logic - processes that we wanted to see in our code.

app.post("/tasks", (req, res) => {
  Task.findOne({ name: req.body.name }, (err, result) => {
    if (result != null && result.name == req.body.name) {
      return res.send("Duplicate task found");
    } else {
      let newTask = new Task({
        name: req.body.name,
      });
      newTask.save((saveErr, savedTask) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send("New task created");
        }
      });
    }
  });
});

app.post("/signup", (req, res) => {
  if (req.body.userName != null || !req.body.password) {
    User.findOne({ userName: req.body.userName }, (err, result) => {
      if (result != null) {
        return res.send("Username already exists.");
      } else {
        let newUser = new User({
          userName: req.body.userName,
          password: req.body.password,
        });
        newUser.save((saveErr, savedUser) => {
          if (saveErr) {
            return console.error(saveErr);
          } else {
            return res.status(201).send("New user successfully registered!");
            return res.send(savedUser);
          }
        });
      }
      // }
    });
  }
});

app.get("/tasks", (req, res) => {
  Task.find({}, (err, result) => {
    if (err) {
      return console.error(err);
    } else {
      return res.status(200).json(result);
    }
  });
});

app.listen(port, () => {
  console.log(`Server running at port ${port}`);
});
